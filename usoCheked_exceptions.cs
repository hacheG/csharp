using System;

namespace aSomeExcep
{
    class theChecked
    {
        static void Main(string[] args)
        {
            Console.WriteLine(int.MaxValue);
            Console.WriteLine(int.MinValue);
            /* 
            checked
            {
                int numero = int.MaxValue;
                int resultado = numero + 20;
                Console.WriteLine(resultado);
            }
           */
            int numero = int.MaxValue;
            //con un check mas dirigido funciona bien
            //int resultado = checked(numero + 20);
            // => o se le permite que pase el overflow, pero solo sirve con int & long
            int resultado = unchecked(numero + 20);
            Console.WriteLine(resultado);
        }

    }
}