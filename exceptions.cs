﻿using System;

namespace excepTryCatch
{
    class tryExc
    {
        static void Main (string[] args)
        {
            int miNumero;
            int intentos = 0;
            Random numero = new Random();
            int aleatorio = numero.Next(0, 100);

            Console.WriteLine("type a numbre among 0 and 100");

            do{
                intentos++;
                
                try
                {
                    miNumero = int.Parse(Console.ReadLine());
                }
                /*catch (Exeption ex) 
                *{
                *    Console.WriteLine("no has intruducido un valor correcto");
                *    miNumero = 0;
                *    Console.WriteLine(ex.Message);
                *}
                */
                catch (FormatException ex)
                {
                    Console.WriteLine("no has intruducido un valor numerico");
                    miNumero = 0;
                    Console.WriteLine(ex.Message);
                }
                catch (OverflowException ex)
                {
                    Console.WriteLine("valor numerico UNICAMENTE de 0 a 100");
                    miNumero = 0;
                    Console.WriteLine(ex.Message);
                }
                
                if(miNumero > aleatorio) Console.WriteLine("the number is low");
                if(miNumero < aleatorio) Console.WriteLine("the number is high");
            }while(miNumero != aleatorio);

            //Console.WriteLine($"you have needed {intentos} attempts");
            Console.WriteLine("you have needed" + intentos + "attempts");

        }
    }
}
