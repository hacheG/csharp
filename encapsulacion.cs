using System.Runtime.InteropServices;
using System;

namespace ejemplosPoo
{
    class OrientedObjects
    {
        static void Main(string[] args)
        {
            Circulo miCircle; //creacion de objeto
            miCircle = new Circulo(); //iniciacion de objeto de tipo circulo

            Circulo circuloDos = new Circulo();
            Console.WriteLine(miCircle.calculoArea(10));
            Console.WriteLine(circuloDos.calculoArea(5));

            ConverterEuroDollar obj1 = new ConverterEuroDollar();
            Console.WriteLine("antes: " + obj1.Convierte(2));
            obj1.CambiaValorEuro(-2.50);
            Console.WriteLine("ahora: "+obj1.Convierte(2));
        }
    }
    class Circulo
    {
        private const double PI = 3.1416; //campo de clase o pripiedad dela clase

        public double calculoArea(int radio) //metodo
        {
            return PI*radio*radio;
        }
    }
    class ConverterEuroDollar
    {
        private double euro = 1.253;

        public double Convierte(double cantidad)
        {
            return cantidad * euro;
        }

        public void CambiaValorEuro(double nuevoValor)
        {
            if (nuevoValor < 0) euro = 1.253;

            else
                euro = nuevoValor;
        }
    }
}