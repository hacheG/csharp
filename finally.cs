using System;

namespace final
{
    class TheFinally
    {
        static void Main(string[] args)
        {
            System.IO.StreamReader archivo = null;

            try
            {
                string linea;
                int contador = 0;
                string path = "/home/hache/Escritorio/tirar.txt";

                archivo = new System.IO.StreamReader(path);
                while((linea = archivo.ReadLine()) != null)
                {
                    Console.WriteLine(linea);
                    contador++;

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("error generico");
                Console.WriteLine(ex.Message);
            
            }
            finally
            {
                if (archivo != null) archivo.Close();
                Console.WriteLine("file closed");
            }
        }
    }
}