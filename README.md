# repo c#
learn and firs pryects

# MODIFICADORES DE ACCESO
## public
accesible desde cualquie parte
## private
accesible desde la propia clase
## protected
accesible desde clase derivada
## internal
accesible desde el mismo ensamblado
## protected internal
accesible desde el mismo ensamblado o clase derivada de otro ensamblado
## private protected
accesible desde la misma clase o clase derivada del mismo ensamblado
## por defecto
accesible desde el mismo paquete