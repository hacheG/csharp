using System;

namespace constructores
{
    class Program
    {
        static void Main(string[] args)
        {
            Coche auto1 = new Coche();
            Coche auto2 = new Coche();
            Coche auto3 = new Coche(1500.8, 0.654);

            Console.WriteLine(auto1.getRuedas());
            Console.WriteLine(auto1.getInfoCoche());
            Console.WriteLine(auto3.getInfoCoche());
        }
    }
    class Coche
    {
        //el constructor da el estado inicial de los objetos
        public Coche(){
            ruedas = 4;
            largo = 2300.5;
            ancho = 0.800;
        }
        public Coche(double largoCoche, double anchoCoche)
        {
            ruedas = 4;
            largo = largoCoche;
            ancho = anchoCoche;
        }
        
        
        private int ruedas;
        private double largo;
        private double ancho;
        private bool climatizador;
        private String tapiceria;
        
        public int getRuedas()
        {
            return ruedas;
        }
        public string getInfoCoche()
        {
            return "ruedas " + ruedas +"\n"+ "largo " + largo +"\n"+ "ancho " + ancho;
        }
        
    }
}