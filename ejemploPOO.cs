using System;

namespace ejemplosPoo
{
    class OrientedObjects
    {
        static void Main(string[] args)
        {
            Circulo miCicle; //creacion de objeto
            miCicle = new Circulo(); //iniciacion de objeto de tipo circulo

            Circulo circuloDos = new Circulo();

            Console.WriteLine(miCicle.pi);
            Console.WriteLine(circuloDos.calculoArea(5));
        }
    }
    class Circulo
    {
        public double pi = 3.1416; //campo de clase o pripiedad dela clase

        public double calculoArea(int radio) //metodo
        {
            return pi*radio*radio;
        }
    }
}